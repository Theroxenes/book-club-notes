25/04/2021 Chapter 1 Notes

Links:
https://crookedtimber.org/2012/05/30/in-soviet-union-optimization-problem-solves-you/
https://thecounter.org/usda-black-farmers-discrimination-tom-vilsack-reparations-civil-rights/
https://growthecon.com/blog/Deep-Roots-1/
https://en.wikipedia.org/wiki/Operation_Ceasefire
https://en.wikipedia.org/wiki/Syncretism

Books:
The Cheese and the Worms
Why Nations Fail
Handcuffed https://www.brookings.edu/book/handcuffed/
The Righteous Mind � Jonathan Haidt
The Cathedral and the Bazaar - https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar

Random notes:
Legibility is a very powerful concept with broad applications, that can tie many independent ideas and fields together.
Local knowledge has value and shouldn�t be destroyed, but synthesized. This is extremely challenging.
Ignoring locality entirely breeds distrust and can result in social decay.
Gay marriage rulings seemed to rapidly shift culture, but similar rulings for trans people do not seem to have had the same effect. Is this because there is more alignment for informal/formal for gay acceptance? 
The internet has allowed for radically greater dispersal of localized knowledge.
�If it weren�t for discord, I don�t know how many people would have interacted with a trans person daily� � Anti
Revealed preferences vs. stated preferences have some parallels to local vs. legible.
The conservative meme of �coastal elites� is rooted in local preferences being uptaken by the state and spread to other localities.
�Are there some local norms which are simply bad and should be done away with, are there some which you should cohere with� � Irk, in relation to gay wedding cake.
Formal vs. informal local rules or norms can have friction with one another. Example: traffic laws are informally ignored a great deal of the time even though they are formally codified. Local enforcement often ignores formal rules in favor of the informal. Misalignment between formal and informal causes issues.
